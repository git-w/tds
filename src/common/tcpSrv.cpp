#include "pch.h"
#include "tcpSrv.h"
#include "logger.h"

#ifdef WINDOWS
#pragma comment(lib, "ws2_32.lib")
#endif

namespace iocpSock {
	std::string format(const char* pszFmt, ...)
	{
		std::string str;
		va_list args;
		va_start(args, pszFmt);
		{
			int nLength = _vscprintf(pszFmt, args);
			nLength += 1;//上面返回的长度是包含\0，这里加上
			std::vector<char> vectorChars(nLength);
			_vsnprintf(vectorChars.data(), nLength, pszFmt, args);
			str.assign(vectorChars.data());
		}
		va_end(args);
		return str;
	}
}

std::vector<tcpSrv*> m_vecTCPIOCP;

void tcpSrv::ListenThread_IOCPServer(LPVOID lpParam)
{
	setThreadName("tcpSrv listen thread");
	tcpSrv* pServ = (tcpSrv*)lpParam;
	if (pServ == NULL)
		return;
	LOG("[detail][tcpServer]listen thread started");

	//等待客户端连接
	SOCKADDR_IN clientAddr;
	int c_size = sizeof(SOCKADDR_IN);
	while (1)
	{
		memset(&clientAddr, 0, sizeof(SOCKADDR_IN));
		SOCKET sClient = accept(pServ->m_sListen, (SOCKADDR*)&clientAddr, &c_size);

		if (pServ->m_bStarted == false)
			break;

		if (sClient != INVALID_SOCKET)//有效连接
		{
			pServ->DoAccept(sClient, &clientAddr);
		}
		else
		{
			int iErr = GetLastError();
			string str = iocpSock::format("[tcpSrv][error]%s Failed accept, error code:%d", pServ->GetIOCPName().c_str(), iErr);
			LOG(str);

			continue;
		}
	}
}

DWORD WINAPI ConnectionMaintainThread(LPVOID lpParam)
{
	tcpSrv* pServ = (tcpSrv*)lpParam;
	setThreadName("tcpSrv connection maintain " + pServ->m_strName);
	pServ->ConnectionMaintain();
	return 0;
}

bool tcpSrv::run(ITcpServerCallBack* pUser, int remotePort, string strLocalIP /*= ""*/)
{
	m_strServerIP = strLocalIP;
	m_iServerPort = remotePort;
	m_pCallBackUser = pUser;

	if (keepAliveTimeout > 0)
	{
		DWORD dwThread = 0;
		HANDLE hThread = CreateThread(NULL, 0, ::ConnectionMaintainThread, (LPVOID)this, 0, &dwThread);
		CloseHandle(hThread);
	}

	string strip = strLocalIP;
	return StartListen(remotePort, strip);
}

void tcpSrv::stop()
{
	LOG("stoping tcpServer...");
	closesocket(m_sListen);
	CloseServer();
	m_bStarted = false;
	LOG("tcpServer stopped");
}


bool tcpSession::send(char* pData, int iLen)
{

	//socket是阻塞式socket，当socket发送缓冲区满时，该函数会阻塞；在发送大数据时会出现阻塞状态
	if (iLen == 0) return false;
	if (sock == 0)
		return false;

	int iRet = ::send(sock, pData, iLen, 0);
	if (iRet == -1)
	{
		int iErr = GetLastError();
		string strError;
		if (iErr == WSAETIMEDOUT)
		{
			strError = "timeout";//客户端不接受数据或者接受处理缓慢可能导致此问题
		}
		else if (iErr == WSAENOTSOCK)
		{
			sock = 0;
		}
		else
		{
			closesocket(sock);
		}
		strError = sys::getLastError();
		string str = iocpSock::format("[tcpSrv][error]send data fail，error=%s,%s:%d", strError.c_str(),remoteIP.c_str(),remotePort);
		logger.logInternal(str);

		if (sock != 0)
		{
			closesocket(sock);
			sock = 0;
		}
	}

	if (iRet > 0)
		iSendSucCount += iLen;
	else
		iSendFailCount += iLen;

	if(iRet > 0)
		GetLocalTime(&stLastActive);

	return iRet > 0;
}

bool tcpSrv::SendData(char* pData, int iLen, string remoteIP)
{
	bool bRet = false;

	std::unique_lock<mutex> lock(m_csClientVectorLock);

	std::vector<COverlappedIOInfo*>::iterator iter = m_vecContInfo.begin();
	//所有该ip的客户端都发送，一个ip有两个连接，发送给了僵尸连接却没有发送给正常连接
	for (; iter != m_vecContInfo.end(); ++iter)
	{
		if ((*iter) && (*iter)->m_cltInfo.remoteIP == remoteIP)
		{
			bRet = (*iter)->m_cltInfo.send(pData,iLen);
		}
	}


	return bRet;
}

//广播发送
bool tcpSrv::SendData(char* pData, int iLen)
{
	std::unique_lock<mutex> lock(m_csClientVectorLock);
	std::vector<COverlappedIOInfo*>& vecClt = m_vecContInfo;
	std::vector<COverlappedIOInfo*>::iterator  iter = vecClt.begin();
	tcpSession* ptrClientInfo = NULL;
	for (; iter != vecClt.end(); ++iter)
	{
		ptrClientInfo = &(*iter)->m_cltInfo;
		ptrClientInfo->send(pData, iLen);
	}
	return true;
}


/*构造函数*/
void tcpSrv::Log(char* sz)
{
	if (pLog)
	{
		pLog(sz);
	}
}

tcpSrv::tcpSrv() : m_strName("TCPIOCP")
, m_strMonitoringIP("")
{
	keepAliveTimeout = 0;
	WSAStartup(MAKEWORD(2, 2), &m_wsaData);

	m_vecTCPIOCP.push_back(this);

	m_bStarted = false;
	m_bStopRecv = false;
	pLog = NULL;
}

/*析构函数*/
tcpSrv::~tcpSrv()
{
	CloseServer();
	WSACleanup();
}

/*TCP Server开启监听*/
bool tcpSrv::StartListen(unsigned short port, string ip)
{
	//listen socket需要将accept操作投递到完成端口，因此listen socket属性必须有重叠IO
	m_sListen = WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);

	if (m_sListen == INVALID_SOCKET)
	{
		return false;
	}
	LOG("[detail][tcpServer]start listen,socket %d created", m_sListen);

	int yes = 1;
	//软件重启后666可能会进入TIME_WAIT状态，立即重启服务器可能会绑定端口失败。SO_REUSEADDR解决该问题
	//SO_EXCLUSIVEADDRUSE避免其他程序使用666端口
	setsockopt(m_sListen, SOL_SOCKET, SO_REUSEADDR, reinterpret_cast<char*>(&yes),sizeof(yes));
	setsockopt(m_sListen, SOL_SOCKET, SO_EXCLUSIVEADDRUSE,reinterpret_cast<char*>(&yes), sizeof(yes));

	//创建并设置IOCP并发线程数量
	if (m_iocp.Create() == false)
	{
		return false;
	}
	if (!m_iocp.AssociateSocket(m_sListen, TYPE_ACP))
	{
		return false;
	}
	LOG("[detail][tcpServer]iocp port created %d,associated with socket", m_iocp.GetIOCP());
	sockaddr_in service;
	service.sin_family = AF_INET;
	service.sin_port = htons(port);
	if (ip.empty())
	{
		service.sin_addr.s_addr = INADDR_ANY;
	}
	else
	{
		service.sin_addr.s_addr = inet_addr(ip.c_str());
	}
	string bindIP = inet_ntoa(service.sin_addr);

	if (::bind(m_sListen, (sockaddr*)&service, sizeof(service)) == SOCKET_ERROR)
	{
		m_lastError = GetLastError();
		LOG("[detail][tcpServer]socket binded to %s:%d fail,errorCode=%d", bindIP,port, m_lastError);
		return false;
	}
	LOG("[detail][tcpServer]socket binded to %s:%d", bindIP.c_str(),port);

	if (listen(m_sListen, SOMAXCONN) == SOCKET_ERROR)
	{
		LOG("[detail][tcpServer]listen at port %d failed",port);
		return false;
	}
	LOG("[detail][tcpServer]listen at port %d success", port);
	//启动工作者线程
	int threadnum = StartWorkThreadPool();
	Sleep(20);

	m_bStarted = true;
	_beginthread(tcpSrv::ListenThread_IOCPServer, 0, this);

	return true;
}

DWORD tcpSrv::SysTime2Unix(SYSTEMTIME& sDT)
{
	tm temptm = { sDT.wSecond, sDT.wMinute, sDT.wHour,
		sDT.wDay, sDT.wMonth - 1, sDT.wYear - 1900, sDT.wDayOfWeek, 0, 0 };
	DWORD iReturn = (DWORD)mktime(&temptm);
	return iReturn;
}

int tcpSrv::CalcTimePassSecond(SYSTEMTIME* stLast, SYSTEMTIME* stNow)
{
	DWORD l = SysTime2Unix(*stLast);
	SYSTEMTIME now;
	if (stNow == NULL)
		stNow = &now;
	GetLocalTime(&now);
	DWORD n = SysTime2Unix(*stNow);

	return (n-l);
}

void tcpSrv::ConnectionMaintain()
{
	LOG("[trace]connection maintain thread started");
	while (1)
	{
		Sleep(5000);

		m_csClientVectorLock.lock();
		std::vector<COverlappedIOInfo*>::iterator iter = m_vecContInfo.begin();
		vector<COverlappedIOInfo*> vecDeadList;
		for (; iter != m_vecContInfo.end(); ++iter)
		{
			tcpSession* pci = &(*iter)->m_cltInfo;
			if (pci->bEnableActivityCheck == false)
				continue;

			int iUnactiveTime = CalcTimePassSecond(&pci->stLastActive);
			if (iUnactiveTime > keepAliveTimeout && keepAliveTimeout != 0)
			{
				vecDeadList.push_back(*iter);
			}
		}
		for (int i = 0; i < vecDeadList.size(); i++)
		{
			COverlappedIOInfo* p = vecDeadList.at(i);
			if (p->m_cltInfo.sock == NULL)
				continue;

			//关闭socket，GetQueuedCompletionStatus返回false,未决的WSA_RECV似乎是丢弃了
			char szLog[100]={0};
			sprintf(szLog,"[trace][tcpSrv]close inactive tcp connection,%s:%d",p->m_cltInfo.remoteIP.c_str(),p->m_cltInfo.remotePort);
			LOG(szLog);
			closesocket(p->m_cltInfo.sock);
			p->m_cltInfo.sock = NULL;

			int iErr = WSAGetLastError();
			string strErrorInfo = "";
		}
		m_csClientVectorLock.unlock();
	}
}


//COverlappedIOInfo 对象必须由WorkThread来删除。如果其他线程删除该对象。GetQueuedCompletionStatus返回会拿到野指针
//因为该对象被 PostRecv放到完成端口中使用了
DWORD WINAPI WorkThread(LPVOID lpParam)
{
	tcpSrv* pServ = (tcpSrv*)lpParam;

	setThreadName("tcpSrv iocp work " + pServ->m_strName);

	while (true)
	{
		DWORD NumberOfBytes = 0;
		ULONG_PTR completionKey = 0;
		OVERLAPPED* ol = NULL;
		//阻塞调用GetQueuedCompletionStatus获取完成端口事件
		//GetQueuedCompletionStatus成功返回
		if (GetQueuedCompletionStatus(pServ->m_iocp.GetIOCP(), &NumberOfBytes, &completionKey, &ol, WSA_INFINITE))
		{
			COverlappedIOInfo* olinfo = (COverlappedIOInfo*)ol;
			if (completionKey == TYPE_CLOSE)
			{
				break;
			}
			//客户端断开连接
			if (NumberOfBytes == 0 && (completionKey == TYPE_RECV || completionKey == TYPE_SEND))
			{
				string str = str::format("[trace][tcpSrv] client actively disconnects socket %s:%d",olinfo->m_cltInfo.remoteIP.c_str(),olinfo->m_cltInfo.remotePort);
				LOG(str);
				pServ->DeleteLink(olinfo->m_socket);
				continue;
			}
			if (completionKey == TYPE_CLOSE_SOCK)
			{
				pServ->DeleteLink(olinfo->m_socket);
				continue;
			}
			switch (completionKey)
			{
			case TYPE_ACP:
			{
			}break;
			case TYPE_RECV:
			{
				//运行到此处，数据已经从socket缓冲区接收到overlapped info中
				olinfo->m_recvBuf.len = NumberOfBytes;
				pServ->DoRecv(olinfo);
				pServ->PostRecv(olinfo);
			}break;
			case TYPE_SEND:
			{
			}break;
			default:
				break;
			}
		}
		else//GetQueuedCompletionStatus出错
		{
			int res = WSAGetLastError();
			//此处olinfo可能出现野指针，后续需要排查原因
			COverlappedIOInfo* olinfo = (COverlappedIOInfo*)ol;
			string connectionInfo;
			if(olinfo)
				connectionInfo = str::format("%s:%d", olinfo->m_cltInfo.remoteIP.c_str(), olinfo->m_cltInfo.remotePort);
			switch (res)
			{
			case ERROR_NETNAME_DELETED:
			{
				string str = "[tcpSrv][trace]client actively disconnects socket " + connectionInfo;
				LOG(str);
			}
			break;
			case ERROR_CONNECTION_ABORTED:
			{
				string strErrorInfo = "server actively disconnects socket" + connectionInfo;
				string str = iocpSock::format("[tcpSrv][trace]%s", strErrorInfo.c_str());
				LOG(str);
			}
			break;
			default:
				break;
			}

			
			if (olinfo)
			{
				pServ->DeleteLink(olinfo->m_socket);
			}
		}
	}

	return 0;
}

/*启动工作者线程池*/
int tcpSrv::StartWorkThreadPool()
{
	DWORD dwThread = 0;
	HANDLE hThread = CreateThread(NULL, 0, WorkThread, (LPVOID)this, 0, &dwThread);
	CloseHandle(hThread);
	return 0;
}

std::string tcpSrv::formatStr(const char* pszFmt, ...)
{
	std::string str;
	va_list args;
	va_start(args, pszFmt);
	{
		int nLength = _vscprintf(pszFmt, args);
		nLength += 1;  //上面返回的长度是包含\0，这里加上
		std::vector<char> vectorChars(nLength);
		_vsnprintf(vectorChars.data(), nLength, pszFmt, args);
		str.assign(vectorChars.data());
	}
	va_end(args);
	return str;
}

bool tcpSrv::DoAccept(SOCKET sockAccept, SOCKADDR_IN* ClientAddr)
{
	//新连接的socket是accept函数默认创建的，因此该socket是阻塞式socket
	COverlappedIOInfo* olinfo = new COverlappedIOInfo();//其他地方内存泄漏很容易导致此处崩溃,原因未知,如果发现此处异常,很可能某处存在内存泄漏
	if (!olinfo) return false;

	//设置发送超时
	int nNetTimeout = 3000;//3秒，由于客户端的异常，没有及时接受数据导致服务端发送缓冲区满了之后，可能导致send超时返回。为防止客户端缺陷不收数据导致服务端send函数永久阻塞，增加该超时设置
	setsockopt(sockAccept, SOL_SOCKET, SO_SNDTIMEO, (char*)&nNetTimeout, sizeof(int));

	//设置closeSocket阻塞等待数据发送完成的超时时间，不设置的话closesocket会立即返回
	struct linger so_linger;
	so_linger.l_onoff = 1;
	so_linger.l_linger = 5; //单位秒
	setsockopt(sockAccept,SOL_SOCKET,SO_LINGER,(char*)&so_linger,sizeof(so_linger));


	olinfo->m_socket = sockAccept;
	olinfo->m_addr = *ClientAddr;
	olinfo->m_cltInfo.pTcpServer = this;
	string remoteIP = formatStr("%d.%d.%d.%d", olinfo->m_addr.sin_addr.S_un.S_un_b.s_b1, olinfo->m_addr.sin_addr.S_un.S_un_b.s_b2, olinfo->m_addr.sin_addr.S_un.S_un_b.s_b3, olinfo->m_addr.sin_addr.S_un.S_un_b.s_b4);
	olinfo->m_cltInfo.remoteIP = remoteIP.c_str();
	olinfo->m_cltInfo.sock = olinfo->m_socket;
	olinfo->m_cltInfo.remotePort = ntohs(olinfo->m_addr.sin_port);

	//服务端只收取recv，同时监听recv和send可用设计位偏移，用或运算实现
	if (m_iocp.AssociateSocket(olinfo->m_socket, TYPE_RECV))
	{
		m_csClientVectorLock.lock();
		m_vecContInfo.push_back(olinfo);
		m_csClientVectorLock.unlock();
		m_pCallBackUser->statusChange_tcpSrv(&olinfo->m_cltInfo, true);
		PostRecv(olinfo);
	}
	else
	{
		DWORD dw = GetLastError();
		delete olinfo;
		return false;
	}

	return true;
}

/*投递一个接收数据完成端口*/
/*
WSARecv 只是向系统提交一个异步接收请求，这个请求会在有数据到达之后返回，并且放入完成队列通知工作线程，这个异步接收请求到此完成，继续提交请求是为了接收下一个数据包，也就是说，每次请求返回之后必须再次提交。

类似这样：
1. 你让一个前台去等待接待一个客人(WSARecv)然后继续做你的事情
2. 你的秘书会一直等着资料夹有新文件然后拿给你然后继续等待有新文件(loop)
3. 前台接待客人之后把客户资料放到资料夹之后就不会继续去前台接待客人了
4. 这个时候如果你不再派前台继续去等待接待客人(WSARecv)，那么你的资料夹不会有新的资料了，这时就需要再次指派前台去等待接待新的客人(再次WSARecv)
*/

bool tcpSrv::PostRecv(COverlappedIOInfo* info)
{
	if (m_bStopRecv)
		return true;

	DWORD BytesRecevd = 0;
	DWORD dwFlags = 0;
	info->ResetOverlapped();
	info->ResetRecvBuffer();
	int recvNum = WSARecv(info->m_socket, &info->m_recvBuf, 1, &info->m_recvBuf.len, &dwFlags, (OVERLAPPED*)info, NULL);

	int iLastError = 0;
	if (recvNum != 0)
	{
		iLastError = WSAGetLastError();
		if (WSA_IO_PENDING != iLastError)
		{
		}
	}
	return true;
}

/*接收数据处理句柄*/
bool tcpSrv::DoRecv(COverlappedIOInfo* info)
{
	info->m_cltInfo.iRecvCount += info->m_recvBuf.len;
	m_pCallBackUser->OnRecvData_TCPServer((char*)info->m_recvBuf.buf, info->m_recvBuf.len, &info->m_cltInfo);
	GetLocalTime(&info->m_cltInfo.stLastActive);
	return true;
}

/*删除失效连接句柄*/
bool tcpSrv::DeleteLink(SOCKET s)
{
	m_csClientVectorLock.lock();
	std::vector<COverlappedIOInfo*>::iterator iter = m_vecContInfo.begin();
	bool found = false;//标志位，客户端在列表中找到与否
	for (; iter != m_vecContInfo.end(); ++iter)
	{
		if ((*iter) && s == (*iter)->m_socket)
		{
			COverlappedIOInfo* ol = *iter;
			m_pCallBackUser->statusChange_tcpSrv(&ol->m_cltInfo, false);
			closesocket(s);
			m_vecContInfo.erase(iter);
			delete ol;
			found = true;//找到了
			break;
		}
	}
	if (!found)//客户端在列表中没有找到
	{
	}
	m_csClientVectorLock.unlock();
	return true;
}

void tcpSrv::disconnect(string remoteAddr)
{
	m_csClientVectorLock.lock();
	if (remoteAddr == "" || remoteAddr == "*")
	{
		for (auto& i : m_vecContInfo)
		{
			closesocket(i->m_cltInfo.sock);
			i->m_cltInfo.sock = 0;
		}
	}
	else
	{
		for (auto& i : m_vecContInfo)
		{
			string tmp = iocpSock::format("%s:%d", i->m_cltInfo.remoteIP.c_str(), i->m_cltInfo.remotePort);
			if (tmp == remoteAddr)
			{
				closesocket(i->m_cltInfo.sock);
				i->m_cltInfo.sock = 0;
			}
		}
	}
	m_csClientVectorLock.unlock();
}

bool tcpSrv::IsIPOnline(string remoteIP)
{
	m_csClientVectorLock.lock();
	std::vector<COverlappedIOInfo*>::iterator iter = m_vecContInfo.begin();
	bool found = false;//标志位，客户端在列表中找到与否
	for (; iter != m_vecContInfo.end(); ++iter)
	{
		if (remoteIP == (*iter)->m_cltInfo.remoteIP)
		{
			found = true;//找到了
			break;
		}
	}
	m_csClientVectorLock.unlock();
	return found;
}

/*关闭服务器*/
void tcpSrv::CloseServer()
{
	//1.清空IOCP线程队列，退出工作线程，给所有的线程发送PostQueueCompletionStatus信息
	if (false == m_iocp.PostStatus(TYPE_CLOSE))
	{
	}

	//3.清空已连接的套接字m_vecContInfo并清空缓存
	std::unique_lock<mutex> lock(m_csClientVectorLock);
	for (int i = 0; i < m_vecContInfo.size(); i++)
	{
		COverlappedIOInfo* olinfo = m_vecContInfo.at(i);
		m_pCallBackUser->statusChange_tcpSrv(&olinfo->m_cltInfo, false); //delete前,必须先通知应用层断开,否则会造成应用层野指针崩溃
		closesocket(olinfo->m_socket);
		delete olinfo;
	}

	m_vecContInfo.clear();
}


void tcpSrv::ClearConnHistoryInfo()
{

}
