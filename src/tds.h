/*
  TDS for iot version 1.0.0
  https://gitee.com/liangtuSoft/tds.git

Licensed under the MIT License <http://opensource.org/licenses/MIT>.
SPDX-License-Identifier: MIT
Copyright (c) 2020-present Tao Lu  

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/




#pragma once
#include <string>
#include <vector>
#include <map>
#include <Windows.h>
using namespace std;



namespace TDS {
	namespace VAL_TYPE {
		const string json = "json";	
		const string Float = "float"; 
		const string integer = "int";
		const string boolean = "bool";
		const string video = "video";
		const string str = "string";
		const string car_strobe = "car_strobe";
		const string man_strobe = "man_strobe";
	};
	


	namespace DEV_ADDR_MODE {
		const string tcpClient = "tcpClient";
		const string tcpServer = "tcpServer";
		const string deviceID = "deviceID";
	}

	namespace CHAN_IO_TYPE {
		const string I = "i";
		const string O = "o";
		const string IO = "io";
	}

	namespace IO_DEV_LEVEL {
		const string server = "server";
		const string gateway = "gateway";
		const string device = "device";
		const string channel = "channel";
	}
};


namespace STORAGE_FMT {
	const string Int16 = "Int16";
	const string UInt16 = "UInt16";
	const string Int32_AB_CD = "Int32 AB CD";
	const string Int32_CD_AB = "Int32 CD AB";
	const string Int32_BA_DC = "Int32 BA DC";
	const string Int32_DC_BA = "Int32 DC BA";
	const string UInt32 = "UInt32";
	const string Int64 = "Int64";
	const string Uint64 = "UInt64";
	const string Float_AB_CD = "Float AB CD";
	const string Float_CD_AB = "Float CD AB";
	const string Float_BA_DC = "Float BA DC";
	const string Float_DC_BA = "Float DC BA";
	const string Double = "Double";
	const string BCD16 = "BCD16";
	const string BCD32 = "BCD32";
}

inline int storageSize(string fmt) {
	if (fmt.find("16") != string::npos)return 2;
	else if (fmt.find("32") != string::npos)return 4;
	else if (fmt.find("64") != string::npos)return 8;
	else if (fmt.find("float") != string::npos || fmt.find("Float") != string::npos)return 4;
	else if (fmt.find("Double") != string::npos || fmt.find("Double")!= string::npos)return 8;
	else {
		return 0;
	}
}

#define STREAM_TYPE_ENUM string
namespace STREAM_TYPE {
	const string bmp = "bmp"; 
	const string h264 = "h264"; 
	const string rgba = "rgba"; 
	const string mono8 = "mono8";
	const string mono16 = "mono16";
}


struct STREAM_INFO {
	int w;
	int h;
	int pixelSize;
	string pixelFmt;
	float frameRate;
	STREAM_INFO()
	{
		w = 0;
		h = 0;
		pixelSize = 0;
		pixelFmt = "";
		frameRate = 0;
	}
};


struct MODULE_BUS_MSG {
	string moduleName;
	string eventName;
	string content; //json
	char* bin; 
};


class RPC_RESP {
public:
	void setResult(string& str) { result = str; }
	void setResult(char* bin, int len) { binResult = new char[len]; memcpy(binResult, bin, len); iBinLen = len; }
	RPC_RESP() {
		result = "";
		binResult = NULL;
		iBinLen = 0;
	}
	~RPC_RESP()
	{
		if (binResult)
			delete binResult;
	}

	string strResp; 
	string error;
	string result;
	string params; 
	char* binResult;
	int iBinLen;
};


typedef void (*fp_ioAddrRecv)(void* user, char* pData, int iLen);
typedef void (*fp_createLicence)();
typedef bool (*fp_rpcHandler)(string strReq, RPC_RESP& resp, string& error);
typedef void(*fp_msgSinker)(MODULE_BUS_MSG& msg);
typedef void (*fp_onVideoStreamRecv)(char* p, int len, STREAM_INFO si, void* user);
typedef void (*fp_procBeforeExit)();

namespace TDS_SESSION_TYPE {
	const string none = "none";
	const string tdsClient = "tdsClient";  
	const string video = "video";
	const string iodev = "ioDev"; 
	const string webHMR = "webHMR"; //web hot module replacement

	//bridge data interfaces
	const string bridgeToLocalCom = "bridgeToLocalCom";
	const string bridgeToiodev = "bridgeToiodev";
	const string bridgeToTcpClient = "bridgeToTcpClient";

	//debug tools
	const string terminal = "terminal";
	const string log = "log";
	const string commpkt = "commpkt"; 
	const string sessionPkt = "sessionPkt";
}

struct ACTIVE_TDS_SESSION {
	string ip;
	int port;
	string type;
};

struct iTDSConf {
	//software conf
	string mode;
	bool debugMode;
	string logLevel;
	bool enableGlobalAlarm;

	//security
	int tokenExpireTime;
	bool enableAccessCtrl;
	string testToken;
	
	//path conf
	string confPath;   //config data path
	string dbPath;     //database folder path
	string uiPath;     //ui web files path

	//tds service conf
	int httpsPort;    //https port of tds;  default value 666; can be upgraded to websocket secure
	int httpPort;  //http port of tds;  default value 667; can be upgraded to websocket
	int httpsPort2;//default 0 not enable; another httpsport
	int httpPort2; //default 0 not enable; another httpport
	bool authDownload;
	int tcpKeepAliveDS;

	//io service conf
	int tdspPort;  //tdsp protocol port of ioServer;  default value 665 
	int mbPort;    //modbus protocol port of ioServer; default value 664
	int iq60Port;  //iq60 protocol port of ioServer; default value 663
	int tcpKeepAliveIO;
	int iotimeoutTdsp; //tdsp comm timeout in milliseconds
	int iotimeoutModbusRtu;
	int iotimeoutIQ60;
	bool enableDevCommReboot;
	bool enableDevReboot;
	int devRebootTime; //seconds
	int devCommRebootTime;

	//3rd party services integration
	string smsApiUser;
	string smsApiKey;
	string smsApiUrl;

	//desktop app mode conf
	bool bConcurrentGateway;
	string dataCenterIp;
	string title;
	string homepage;
	string uiMode;
	string uiTitle;
	bool fullscreen; 
	bool singleGenicamHost;
	vector<ACTIVE_TDS_SESSION> vecActiveSession;

	//module enable/disable
	bool enableLog;
	bool enableDB;
	bool enableScript;

	//tds edge conf
	bool edge; //tds edge gateway mode
	string cloudIP;
	int cloudPort;
	string deviceID;

	//debug
	bool bCreateDumpWhenLogError;
	bool bStopCycleAcq;

	virtual int getInt(string key, int iDef) = 0;
	virtual string getStr(string key, string sDef) = 0;
};


enum RPC_ERROR_CODE {
	//json rpc standard
	TDS_ERROR_CODE = -32603,

	//common
	TEC_FAIL = -40000,
	TEC_InvalidReqFmt = -40001,
	TEC_WrongParamFmt = -40002,
	TEC_TIME_SELECTOR_FMT_ERROR = -40003,
	TEC_TAG_SELECTOR_FMT_ERROR = -40004,
	TEC_paramMissing = -40021,

	//user
	AUTH_tokenError = -40101,
	AUTH_tokenMissing = -40102,
	AUTH_userNotFound = -40103,
	AUTH_userMissing = -40104,
	AUTH_passwordError = -40105,
	AUTH_signatureMissing = -40106,

	//mo
	MO_specifiedTagNotFound = -40201,
	MO_outputFail = -40202,
	MO_outputValNotSpecified = -40203,
	MO_outputValShouldBeBool = -40204,
	MO_outputValShouldBeNumber = -40205,
	MO_currentValIsNull = -40206,
	MO_outputTimeout = -40207,
	TEC_VAL_TYPE_ERROR = -40208,

	//io
	IO_devNotFound = -40301,
	IO_devOffline = -40302,
	IO_reqTimeout = -40303,
	IO_devTypeError = -40304,
	IO_ioAddrNotSpecified = -40305,
	IO_chanTemplateNotFound = -40306,

	//video
	TEC_VIDEO_PARAM_NOT_VALID = -40401,
	TEC_NO_STREAM_SRC = -40402,
	TEC_STREAM_ID_NOT_FOUND = -40403,

	//tdsp
	DEV_confNameNotFound = -42001,
	DEV_confCategoryNotFound = -42002,
	DEV_chanNotFound = -42003,

	//os
	OS_fileNotExist = -43001,

	//alarm
	ALM_alarmEventNotFound = -44001
};

//code ,msg is specified by JSON RPC stardard. desc is for detail description by TDS.can be Chinese Charactors
inline string makeRPCError(int code, string msg,string desc = "")
{
	string error = "{\"code\":" + std::to_string(code) + ",\"message\":\"" + msg + "\"";
	if (desc != "")
	{
		string data = ",\"data\":{\"desc\":\"" + desc + "\"}";
		error += data;
	}
	error += "}";

	return error;
}

class i_tdsPlugin {
public:
	virtual bool init() = 0;
	virtual bool run() = 0;
};


//interface of tds.db
//key is timestamp as 2020-01-01 11:11:11,value is a json string of one data element
#define DB_DATA_SET std::map<string,string>
class i_database {
public:
	//crud options
	//virtual void INSERT(string strTag, SYSTEMTIME stTime, json& jData, json dataFile = nullptr) = 0;
	//time: "2020-02-14~2020-02-15" or "1d1h1m30s"
	//filter: "humidiy==55 && temperature>30"
	//dataSet is json de array
	//virtual bool SELECT(string tag, TIME_SELECTOR& timeSelector, string filter, DB_DATA_SET& result) = 0;
	virtual bool Update(string tag, SYSTEMTIME stTime, string& sData) = 0;


	
	//deFileUrl  1.localfile 2.localfolder 3.http url
	virtual void saveDEFile(string strTag, SYSTEMTIME stTime, string deFileUrl) = 0;

	//get db.json path
	virtual string getPath_dbFile(string strTag, SYSTEMTIME date) = 0;
	//de folder path
	virtual string getPath_dataFolder(string strTag, SYSTEMTIME date) = 0;
	virtual string getPath_deFile(string strTag, SYSTEMTIME stTime) = 0;
	virtual string getPath_dbRoot() = 0;
};


class i_xiaoT : public i_tdsPlugin {
public:
	virtual std::string getReply(string msg) = 0;
};

class i_gzhServer : public i_tdsPlugin {
public:
	virtual std::string getReply(string msg) = 0;
};

class i_smsServer : public i_tdsPlugin {
public:
	virtual bool send(string& msg,string& phoneNum) = 0;
};


typedef void (*fp_toolRun)();

//interface of TDS
class i_tds {
public:
	virtual string getVersion() = 0;
	virtual bool setEncodeing(string encoding) = 0; // utf8 or gb2312
	virtual bool run(string cmdline = "") = 0;
	virtual void stop() = 0;
	virtual bool setProcBeforeExit(fp_procBeforeExit callback) = 0;
	fp_procBeforeExit m_fpProcBeforeExit;
	fp_createLicence createLicence;
	virtual bool call(string method, string param, RPC_RESP& resp) = 0;
	virtual void callAsyn(string method, string param) = 0;
	virtual void rpcNotify(string method, string params = "", string sessionId = "") = 0;


	virtual void setRpcHandler(fp_rpcHandler handler) = 0;


	virtual bool enableIoLog(string ioAddr, bool bEnable) = 0;
	virtual bool sendToIoAddr(string ioAddr, const char* p,int l) = 0;
	virtual bool connectDev(string ioAddr) = 0; 
	virtual bool isOnline(string ioAddr) = 0;
	virtual bool isConnected(string ioAddr) = 0;
	virtual bool isInUse(string ioAddr) = 0;
	virtual bool lockIoAddr(string ioAddr) = 0;
	virtual bool unlockIoAddr(string ioAddr) = 0;
	virtual bool setIoAddrRecvCallback(string ioAddr, void* user, fp_ioAddrRecv recvCallback) = 0;

	//video function
	virtual void startStream(string streamId, STREAM_INFO* si=NULL) = 0;
	//push to sepecified streamId 
	virtual void pushStream(string streamId, char* pData, int len, STREAM_INFO* si=NULL) = 0;
	virtual void pullStream(string streamId, void* user, fp_onVideoStreamRecv onRecvStream, STREAM_INFO*si = NULL) = 0;


	virtual void log(const char* text) = 0;

	//event bus
	virtual void registerMsgSinker(fp_msgSinker sinker) = 0;
	virtual void publishMsg(MODULE_BUS_MSG& msg) = 0;

	
	iTDSConf* conf;
	i_database* db;
	i_xiaoT* xiaoT;
	i_gzhServer* gzhServer;
	i_smsServer* smsServer;
	i_tdsPlugin* shellServer;

	SYSTEMTIME stStartupTime;

	HWND uiWnd;
	string uiWndTitle;

	map<string, fp_toolRun> tools;//注册到tds总线的工具插件
};


#ifndef _TDS

typedef i_tds* (*fp_getTds)();

inline i_tds* getITDS() {
	HMODULE hMod = LoadLibrary("tds.dll");
	if (hMod)
	{
		fp_getTds pGetTds = (fp_getTds)GetProcAddress(hMod, "getTds");
		if (pGetTds)
		{
			return pGetTds();
		}
	}
	return NULL;
}

#endif

extern i_tds* tds;