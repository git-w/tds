#include "pch.h"
#include "tdsSession.h"
#include "mp.h"
#include "logger.h"
#include "ioDev.h"
#include "rpcHandler.h"
#include "ioSrv.h"
#include "webSrv.h"
#include "httplib.h"
#include "streamServer.h"


TDS_SESSION::TDS_SESSION()
{
    Init();
}

TDS_SESSION::~TDS_SESSION()
{
}

TDS_SESSION::TDS_SESSION(tcpSession* p)
{
    Init();
    GetLocalTime(&stCreateTime);
    bConnected = true;
    pTcpSession = p;
    sock = p->sock;
    port = p->remotePort;
    ip = p->remoteIP;
    p->pALSession = this;
    type = TDS_SESSION_TYPE::iodev;
}

TDS_SESSION::TDS_SESSION(tcpSessionClt* p)
{
    Init();
    m_bActiveSession = true;
    bConnected = true;
    pTcpSessionClt = p->tcpClt;
    sock = p->sock;
    port = p->srvPort;
    ip = p->srvIP;
    p->pALSession = this;
    type = TDS_SESSION_TYPE::iodev;
}

RPC_SESSION TDS_SESSION::getRpcSession()
{
    RPC_SESSION s = *this;
    s.remoteAddr = getRemoteAddr();
    return s;
}

string TDS_SESSION::getId()
{
    return getRemoteAddr();
}

string TDS_SESSION::getRemoteAddr()
{
    unique_lock<recursive_mutex> lock(m_mutexTcpLink);//使用tcplink
    if (pTcpSession)
    {
       return pTcpSession->remoteIP + ":" + str::fromInt(pTcpSession->remotePort);
    }
    return "";
}

bool TDS_SESSION::getTcpSession(tcpSession& ts)
{
    std::unique_lock<recursive_mutex> lock(m_mutexTcpLink);
    if (pTcpSession)
    {
        ts = *pTcpSession;
        return true;
    }
    else
    {
        return false;
    }
}

void TDS_SESSION::Init()
{
    m_bSingleDevMode = false;
    m_bAppDataRecved = false;
    abandonLen = 0;
    m_bStateLessSession = false;
    m_bActiveSession = false;
    m_bNeedLog = true;
    bConnected = false;
    pTcpSession = NULL;
    pTcpSessionClt = NULL;
    role = "";
    encode = "utf8";
    type = "";
    name = "";
    bInitSegSended = false;
    bridgedTcpCltHandler.pTdsSession = this;
    videoServiceNode = NULL;
    sock = 0;
    iTLProto = TRANSFER_LAYER_PROTO_TYPE::TLT_UNKNOWN;
    iALProto = APP_LAYER_PROTO::UNKNOWN;
    pTcpSession = NULL;
    m_alBuf.Init();
    mapTagDataSubscribe.clear();
    bSubAll = false;
    bInitSegSended = false;
    m_IoDev = NULL;
}

bool TDS_SESSION::isConnected()
{
    std::unique_lock<recursive_mutex> lock(m_mutexTcpLink);
    return bConnected;
}

bool TDS_SESSION::disconnect()
{
    std::unique_lock<recursive_mutex> lock(m_mutexTcpLink);//使用tcplink
    if (pTcpSession)
    {
        closesocket(pTcpSession->sock);
    }
    else if (pTcpSessionClt)
    {
        closesocket(pTcpSessionClt->m_session.sock);
    }
    return true;
}

string TDS_SESSION::GetClientIp()
{
    if (pTcpSessionClt)
        return pTcpSessionClt->m_remoteIP;
    return "";
}

void TDS_SESSION::statisOnSend(char* p, int len,bool success)
{
    {
        shared_lock<shared_mutex> lock(csSessionPktSessions);
        if (sessionPktSessions.size() == 0)
            return;
    }
    

    //监视会话的数据包不记录日志
    //if (type == TDS_SESSION_TYPE::sessionPkt ||
    //    type == TDS_SESSION_TYPE::commpkt ||
    //    type == TDS_SESSION_TYPE::log ||
    //    type == TDS_SESSION_TYPE::video) //sessionPkt自己的日志不记录
    //{
    //    return;
    //}


    //仅监视io数据包
    if (type.find(TDS_SESSION_TYPE::iodev) == string::npos)
    {
        return;
    }
    if (!m_bNeedLog)
        return;

    json j;
    SYSTEMTIME st;
    GetLocalTime(&st);
    j["time"] = timeopt::st2strWithMilli(st);
    j["remoteAddr"] = getRemoteAddr();
    if(success)
        j["type"] = "发送成功";
    else
        j["type"] = "发送失败";
    j["len"] = len;
    j["data"] = str::bytesToHexStr(p, len);
    j["sessionType"] = type;
    string s = j.dump(4);

    sendToSessionPktSessions((char*)s.c_str(), s.length());
}


void TDS_SESSION::statisOnRecv(char* p, int len)
{
    {
        shared_lock<shared_mutex> lock(csSessionPktSessions);
        if (sessionPktSessions.size() == 0)
            return;
    }
    //监视会话的数据包不记录日志
    //if (type == TDS_SESSION_TYPE::sessionPkt ||
    //    type == TDS_SESSION_TYPE::commpkt ||
    //    type == TDS_SESSION_TYPE::log ||
    //    type == TDS_SESSION_TYPE::video) //sessionPkt自己的日志不记录
    //{
    //    return;
    //}

    //仅监视io数据包
    if (type.find(TDS_SESSION_TYPE::iodev) == string::npos)
    {
        return;
    }

    try {
        json j;
        SYSTEMTIME st;
        GetLocalTime(&st);
        j["time"] = timeopt::st2strWithMilli(st);
        j["remoteAddr"] = getRemoteAddr();
        j["type"] = "接收";
        j["len"] = len;
        //j["data"] = str::fromBuff(p, len);
        j["data"] = str::bytesToHexStr(p, len);
        j["sessionType"] = type;
        string s = j.dump();
        sendToSessionPktSessions((char*)s.c_str(), s.length());
    }
    catch (std::exception& e)
    {
        LOG("[error]接收到非utf8字符串,tdsSession=%s,%s", getRemoteAddr().c_str(), e.what());
    }
}



 int TDS_SESSION::send(char* p,int len,bool bNeedLog){
     GetLocalTime(&lastSendTime);
     int iSend = 0;

     if (sockPipe != 0)
     {
         iSend = WebServer::sendToWs(p, len, sockPipe);
     }
     else if (pTcpSessionClt)
     {
         iSend = pTcpSessionClt->SendData(p, len);
     }
     else
     {
         unique_lock<recursive_mutex> lock(m_mutexTcpLink);//使用tcplink
         if (pTcpSession) // means lower layer has been disconneted
             iSend = ds.SendAppLayerData(p, len, this);
     }


     if (bNeedLog)
         statisOnSend(p, len,iSend>0);
     return 0;
 }

 int TDS_SESSION::sendStr(string str, bool bNeedLog)
 {
     return send((char*)str.c_str(),str.length(),bNeedLog);
 }

 int TDS_SESSION::getSendedBytes()
 {
     std::unique_lock<recursive_mutex> lock(m_mutexTcpLink);//使用tcplink
     if (pTcpSession)
     {
         return pTcpSession->iSendSucCount;
     }
     else if (pTcpSessionClt)
     {
         return pTcpSessionClt->m_session.iSendSucCount;
     }
     return 0;
 }

 int TDS_SESSION::getRecvedBytes()
 {
     std::unique_lock<recursive_mutex> lock(m_mutexTcpLink);//使用tcplink
     if (pTcpSession)
     {
         return pTcpSession->iRecvCount;
     }
     else if (pTcpSessionClt)
     {
         return  pTcpSessionClt->m_session.iRecvCount;
     }
     return 0;
 }

 ioDev* TDS_SESSION::getIODev(string ioAddr)
 {
     for (int i = 0; i < m_vecIoDev.size(); i++)
     {
         string p = m_vecIoDev.at(i);
         if (p == ioAddr)
         {
             return ioSrv.getIODev(ioAddr);
         }
     }
     return nullptr;
 }

 void TDS_SESSION::CBridgedTcpClientHandler::statusChange_tcpClt(tcpSessionClt* connInfo, bool bIsConn)
 {
 }

 void TDS_SESSION::CBridgedTcpClientHandler::OnRecvData_TCPClient(char* pData, int iLen, tcpSessionClt* connInfo)
 {
     pTdsSession->send(pData, iLen);
 }

 

 void TDS_SESSION::onTcpDisconnect()
 {
     //p->pTcpSession is a tcpSession will be deleted after statusChange_tcpSrv callback
     //but TDS_SESSION is not deleted until all users release it
     //so here p->pTcpSession is set to none
     unique_lock<recursive_mutex> lock(m_mutexTcpLink);//修改tcplink
     dsCltStream = nullptr;
     pTcpSession = nullptr;
     pTcpSessionClt = nullptr;
     if (pBridgedTcpClient)
     {
         delete pBridgedTcpClient;
     }
     if (m_IoDev)
     {
         m_IoDev->bindIOSession(NULL);
         m_IoDev->setOffline();
         logger.logInternal("[ioDev]设备掉线,ioAddr=" + m_IoDev->getIOAddrStr() + ",tag=" + m_IoDev->m_strTagBind);
         m_IoDev = NULL;
     }
     if (bridgedIoSession)
     {
         bridgedIoSession->bridgedIoSessionClient = NULL;
         bridgedIoSession = NULL;
     }
     if (bridgedIoSessionClient)
     {
         bridgedIoSessionClient->bridgedIoSession = NULL;
         bridgedIoSessionClient = NULL;
     }

     videoServiceNode = NULL;
     bConnected = false;
     m_fileUploader.stopWrite();

     //暂时取消1个链接上线多台设备机制
     //此处ioSrv.getIODev会锁住ioSrv，如果此时刚好进行doCycleTask周期采集。会导致死锁
     // m_mutexTcpLink 套 ioSrv.m_csThis  和 ioSrv.m_csThis 套m_mutexTcpLink导致。doCycleTask后面的发送会锁m_mutexTcpLink
     //for (int i = 0; i < m_vecIoDev.size(); i++)
     //{  
     //    string ioAddr = m_vecIoDev[i];
     //    ioDev* p = ioSrv.getIODev(ioAddr);
     //    if (p)
     //    {
     //        logger.logInternal("[ioDev]设备掉线,ioAddr=" + ioAddr + ",tag=" + p->m_strTagBind);
     //        p->setOffline();
     //    }
     //}
 }

 void TDS_SESSION::setActivityCheck(bool bEnable)
 {
     if (pTcpSession)
     {
         pTcpSession->bEnableActivityCheck = bEnable;
     }
 }

 bool FILE_WRITER::startWrite(string path, long len)
 {
     if (fp)
     {
         stopWrite();
     }

     totalLen = len;
     filePath = path;
     fs::createFolderOfPath(path);
     wstring wpath = charCodec::autoToUtf16(path);

     fp = _wfopen(wpath.c_str(), L"wb");
     if (fp)
     {
         return true;
     }
     else
     {
         int  iError = GetLastError();
         std::cout << "open file failed,error=" << iError << "," << path << std::endl;
     }
     return false;
 }

 void FILE_WRITER::stopWrite()
 {
     if (fp)
     {
         fclose(fp);
         fp = nullptr;
     }
 }

 long FILE_WRITER::write(char* pData, long iLen)
 {
     if (fp)
     {
         long leftLen = totalLen - writedLen;
         long wlen = leftLen > iLen ? iLen : leftLen;

         fwrite(pData, 1, wlen, fp);
         writedLen += wlen;
         
         if (writedLen == totalLen)
         {
             stopWrite();
         }
         else if (writedLen > totalLen)
             assert(false);

         return wlen;
     }
     return 0;
 }

 bool bTestStream = false;
 int ThreadfMp4OverWS(std::shared_ptr<TDS_SESSION> pTestSess) {
     bTestStream = true;
     string str = fs::appPath() + "\\test.mp4";
     //string str = "E:\\VideoTest\\1.avi";
     char* pData = NULL;
     int iDataLen = 0;
     FILE* f = fopen(str.c_str(), "rb");
     if (f)
     {
         fseek(f, 0, SEEK_END);
         iDataLen = ftell(f);
         pData = new char[iDataLen];
         fseek(f, 0, SEEK_SET);
         fread(pData, 1, iDataLen, f);
         fclose(f);
     }
     if (iDataLen == 0)
     {
         bTestStream = false;
         return 0;
     }

     Sleep(500);
     while (pTestSess->type == TDS_SESSION_TYPE::video)
     {
         pTestSess->type = TDS_SESSION_TYPE::video;
         for (int i = 0; i < iDataLen;)
         {
             int iSend = 20000;
             if (i + iSend > iDataLen)
                 iSend = iDataLen - i;
             if (!pTestSess->send(pData + i, iSend))
             {
                 pTestSess->type = TDS_SESSION_TYPE::none;
                 break;
             }
             i += iSend;
             Sleep(40);
         }
     }
     pTestSess->type = TDS_SESSION_TYPE::none;
     delete pData;
     bTestStream = false;
     return 0;
 }

 void dataServer::initWsSessionInfo(string& strData, std::shared_ptr<TDS_SESSION> tdsSession)
 {
     //terminal可以用来打开与某一接口的透传桥接，并发送指令
     if (strData.find("/terminal") != string::npos)
     {
         int pos = strData.find("terminal");
         int pos1 = strData.find(" ", pos);
         string ioAddr = strData.substr(pos + 9, pos1 - (pos + 9));
         ioDev* p = ioSrv.getIODev(ioAddr);
         if (p && p->pIOSession != NULL)
         {
             tdsSession->type = TDS_SESSION_TYPE::bridgeToiodev;
             tdsSession->bridgedIoSession = p->pIOSession;
             p->pIOSession->bridgedIoSessionClient = tdsSession;
             LOG("open websocket terminal at ioAddr %s success", ioAddr.c_str());
             tdsSession->setActivityCheck(false);
             tdsSession->bridgedIoSession->setActivityCheck(false);
         }
         else if (p && p->m_devType == IO_DEV_TYPE::GW::local_serial)
         {
             tdsSession->setActivityCheck(false);
             tdsSession->type = TDS_SESSION_TYPE::bridgeToLocalCom;
             tdsSession->bridgedLocalCom = ioAddr;
             p->pSessionClientBridge = tdsSession;
         }
         else
         {
             closesocket(tdsSession->sock);
             return;
         }
     }
     else if (strData.find("/COM") != string::npos)
     {
         int pos = strData.find("COM");
         int pos1 = strData.find(" ", pos);
         string portNum = strData.substr(pos, pos1 - pos);
         ioDev* p = ioSrv.getIODev(portNum);
         tdsSession->type = TDS_SESSION_TYPE::bridgeToLocalCom;
         tdsSession->setActivityCheck(false);
         if (p)
         {
             tdsSession->bridgedLocalCom = portNum;
             p->pSessionClientBridge = tdsSession;
         }
         else
         {
             //string html = portNum + " is not in the opened port list,please open it first";
             //std::string header = "HTTP/1.1 200 OK\r\n";
             //header += "Content-Type: text/html; charset=utf-8\r\n";
             //header += "Accept-Ranges: none\r\n"; // no support for partial requests
             //header += "Cache-Control: no-store, must-revalidate\r\n";
             //header += "Content-Length: " + std::to_string(html.length()) + "\r\n";
             //header += "\r\n";

             //string resp = header + html;
             //send(tdsSession->sock, (char*)resp.data(), resp.length(), 0);
             closesocket(tdsSession->sock);
             return;
         }
     }
     else if (strData.find("tcp") != string::npos)
     {
         int pos = strData.find("tcp");
         int pos1 = strData.find(" ", pos);
         string host = strData.substr(pos + 4, pos1 - (pos + 4));
         tdsSession->pBridgedTcpClient = new tcpClt();
         tdsSession->type = TDS_SESSION_TYPE::bridgeToTcpClient;
         tdsSession->setActivityCheck(false);
         if (tdsSession->pBridgedTcpClient->connect(&tdsSession->bridgedTcpCltHandler, host))
         {
             LOG("bridge websocket to tcp %s success", host.c_str());
         }
         else
         {
             LOG("bridge websocket to tcp %s fail", host.c_str());
             delete tdsSession->pBridgedTcpClient;
             tdsSession->pBridgedTcpClient = NULL;
             closesocket(tdsSession->sock);
             return;
         }
     }
     else if (strData.find("/log") != string::npos)
     {
         tdsSession->type = TDS_SESSION_TYPE::log;
         logTdsSessions.push_back(tdsSession);
         logger.logOutput = logToWebsock;
         tdsSession->setActivityCheck(false);
     }
     else if (strData.find("/sessionpkt") != string::npos)
     {
         tdsSession->type = TDS_SESSION_TYPE::sessionPkt;
         sessionPktSessions.push_back(tdsSession);
         tdsSession->setActivityCheck(false);
     }
     else if (strData.find("/commpkt") != string::npos)
     {
         tdsSession->type = TDS_SESSION_TYPE::commpkt;
         commpktSessions.push_back(tdsSession);
         tdsSession->setActivityCheck(false);
     }
     else if (strData.find("teststream") != string::npos && !bTestStream)
     {
         tdsSession->type = TDS_SESSION_TYPE::video;
         std::thread t(ThreadfMp4OverWS, tdsSession);
         t.detach();
     }
     else if (strData.find("desktop") != string::npos)
     {
         tdsSession->type = TDS_SESSION_TYPE::video;
#ifdef ENABLE_FFMPEG
         rds.startStream(tdsSession);
#endif
     }
     else if (strData.find("stream") != string::npos)
     {
         int pos = strData.find("stream");
         map<string, string> mapParams;
         getUrlParams(strData, mapParams);
         string streamId; //支持码流的tag
         string fmt = ""; //为空，则图像不进行任何转换直接发送
         int frameRate = 0;
         if (mapParams.size() > 0)
         {
             if (mapParams.find("streamId") != mapParams.end())
             {
                 streamId = mapParams["streamId"];
             }
             if (mapParams.find("fmt") != mapParams.end())//fmt is not specified
             {
                 fmt = mapParams["fmt"];
             }
             if (mapParams.find("frameRate") != mapParams.end())//fmt is not specified
             {
                 frameRate = str::toInt(mapParams["frameRate"]);
             }
             streamId = httplib::detail::decode_url(streamId, false);


             if (streamId != "")
             {
                 streamSrvNode* pVsn = streamSrv.getSrvNode(streamId);
                 if (pVsn)
                 {
                     tdsSession->videoServiceNode = pVsn;
                     tdsSession->type = TDS_SESSION_TYPE::video;
                     STREAM_INFO si;
                     si.pixelFmt = fmt;
                     si.frameRate = frameRate;
                     pVsn->addPuller(tdsSession, &si);
                     string szLog = "[Session会话][开始] 类型:" + tdsSession->type + " 码流ID:" + streamId + " 格式:" + fmt + ",客户端地址:" + tdsSession->ip + ":" + str::fromInt(tdsSession->port);
                     LOG(szLog);
                 }
             }
         }
     }
     else //连接根地址 默认为rpc连接
     {
         if (strData.find("tdsClient") != string::npos)
         {

         }

         map<string, string> mapParams;
         getUrlParams(strData, mapParams);
         if (mapParams.find("needLog") != mapParams.end())
         {
             string needLog = mapParams["needLog"];
             if (needLog == "0")
             {
                 tdsSession->m_bNeedLog = false;
             }
         }

         if (tds->conf->debugMode)
         {
             string s = R"(
						{
							"jsonrpc": "2.0", 
							"method": "notify.close_heartbeat", 
							"params": {
							}, 
							"id": null
						}
					)";
             tdsSession->send((char*)s.data(), s.length());
         }
         if (tdsSession->type == "")
             tdsSession->type = TDS_SESSION_TYPE::tdsClient;
         tdsSession->iALProto = "tdsRPC";

         string szLog = "[websocket会话][开始] 类型:" + tdsSession->type + ",地址:" + tdsSession->ip + ":" + str::fromInt(tdsSession->port);
         LOG(szLog);
     }
 }
